package Server;

import java.net.*;

public class Server {

    private Socket socket;
    private int clientNumber;

    public static void main(String[] args) throws Exception {
        System.out.println("The Hangman server is running.");
        System.out.println("Waiting for playes to connect...");
        int clientNumber = 0;
        ServerSocket listener = null;
        try {
            
            listener = new ServerSocket(7410);
            
            while (true) {
                Game game = new Game();
                Game.Player player0 = game.new Player( listener.accept(), 0 );
                Game.Player player1 = game.new Player( listener.accept(), 1 );
                
                game.setCurrentPlayer(player0);
                
                player0.setOpponent(player1);
                player1.setOpponent(player0);
                
                player0.start();
                player1.start();
            }
        } finally {
            listener.close();
        }
    }

}