package Server;

import java.util.*;
import java.net.*;
import java.io.*;

/**
 *
 * @author Fredrik
 */
public class Game {

    private String word;
    private List<String> guessed;
    private boolean winner;

    private Player currentPlayer;

    public Game() {
        guessed = new ArrayList<>();
        
        try {
            
            BufferedReader reader = new BufferedReader( new FileReader("ordlista.txt") );
            String line = reader.readLine();
            
            List<String> lines = new ArrayList<>();
            while (line != null) {
                lines.add(line);
                line = reader.readLine();
            }
            Random r = new Random();
            word = lines.get(r.nextInt(lines.size()));
        }
        catch(Exception e) {
            System.out.println( e );
        }
        //word = "test";
    }

    public synchronized boolean testLetter(String letter) {

        for (int i = 0; i < word.length(); i++) {
            if (Character.toString(word.charAt(i)).equals(letter)) {
                return true;
            }
        }

        return false;
    }
    
    public String getCensoredWord() {
        
        String censoredWord = "";
        
        for (int i = 0; i < word.length(); i++) {
            String currentLetter = Character.toString(word.charAt(i));
            censoredWord += !guessed.contains( currentLetter ) ? "*" : currentLetter;
        }
        
        return censoredWord;
    }

    public boolean isWordComplete() {
        for (int i = 0; i < word.length(); i++) {
            if (!guessed.contains(Character.toString(word.charAt(i)))) {
                return false;
            }
        }
        return true;
    }

    public synchronized boolean guessLetter(String letter) {
        
        letter = letter.toLowerCase();
        
        if (!guessed.contains(letter)) {
            guessed.add(letter);
            currentPlayer.opponent.otherPlayerGuessed(letter);
            return true;
        } else {
            return false;
        }
    }

    public synchronized Player getCurrentPlayer() {
        return currentPlayer;
    }

    public synchronized void setCurrentPlayer(Player player) {
        System.out.println(player.toString());
        currentPlayer = player;
    }

    public class Player extends Thread {

        private int id;
        private Socket socket;
        private BufferedReader input;
        private PrintWriter output;

        private Player opponent;

        public Player(Socket socket, int id) {
            this.socket = socket;
            this.id = id;

            try {
                input = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
                output = new PrintWriter(this.socket.getOutputStream(), true);
                System.out.println("New connection with client# " + this.id + " at " + socket);
                output.println("WELCOME Welcome Player " + this.id);
                output.println("MESSAGE Waiting for one more player to connect...");
            } catch (IOException e) {
                System.out.println("Player lost connection: " + e);
            }
        }

        public void setOpponent(Player opponent) {

            this.opponent = opponent;
        }

        @Override
        public void run() {

            try {

                output.println("MESSAGE All players connected"); 
                output.println("CENSORED_WORD " + getCensoredWord() );
                
                while (true) {
                    
                    if (getCurrentPlayer() == this && !isWordComplete() ) {
                        
                        //output.println("CENSORED_WORD " + getCensoredWord() );
                        output.println("GUESS"); // Prompt the user to guess a letter

                        String command = input.readLine();
                        
                        if (command.startsWith("GUESS")) {

                            String letter = command.substring(6, 7);
                            
                            if (guessLetter(letter)) {
                                
                                output.println("CENSORED_WORD " + getCensoredWord() );
                                
                                if (testLetter(letter)) {
                                    output.println(isWordComplete() ? "VICTORY" : "CORRECT");
                                } else {
                                    output.println("INCORRECT");
                                    setCurrentPlayer(this.opponent);
                                }
                            } else {
                                output.println("ALREADY_EXISTS");
                            }
                        } else if (command.startsWith("QUIT")) {
                            return;
                        }
                        
                        output.flush();
                    }
                }

            } catch (Exception e) {
            } finally {
                try {
                    socket.close();
                } catch (IOException e) {
                    System.out.println(e);
                }
            }
        }

        public void otherPlayerGuessed(String letter) {
            output.println("CENSORED_WORD " + getCensoredWord() );
            output.println("OPPONENT_GUESSED " + letter);
            if (testLetter(letter)) {
                output.println(isWordComplete() ? "LOST" : "CORRECT " + letter);
            } else {
                output.println("INCORRECT " + letter);
            }

        }

        @Override
        public String toString() {
            return "Player " + String.valueOf(id);
        }

    }

}
