package Client;

import java.net.Socket;
import java.io.*;
import java.util.Scanner;

public class Client {
    
    private final int PORT;
    private Socket socket;
    private BufferedReader input;
    private PrintWriter output;
    private Scanner scanner;
    
    public Client() throws Exception {
        
        this.PORT = 7410;
        this.socket = new Socket("localhost", this.PORT);
        this.input = new BufferedReader( new InputStreamReader( socket.getInputStream() ) );
        this.output = new PrintWriter( socket.getOutputStream(), true );
        this.scanner = new Scanner(System.in, "iso-8859-1");
        
    }
    
    public void play() throws Exception {
        
        try {
            
            while(true) {
                
                String response = input.readLine();
                
                if( response.startsWith("WELCOME")) {  
                    System.out.println( response.substring(8) );
                }
                else if( response.startsWith("MESSAGE")) {
                    System.out.println( response.substring(8) );
                } 
                else if( response.startsWith("GUESS")) {
                    System.out.print("Guess a letter: ");
                    output.println( "GUESS " + scanner.nextLine() );
                } 
                else if( response.startsWith("OPPONENT_GUESSED") ){
                    System.out.println( "The opponent guessed '" + response.substring(17) + "'" );
                } 
                else if( response.startsWith("VICTORY") ) {
                    System.out.println( "Congratulations, correct word!" );
                    output.println("QUIT");
                    break;
                }
                else if( response.startsWith("LOST") ) {
                    System.out.println( "Better luck next time, the opponent guessed the correct word" );
                    output.println("QUIT");
                    break;
                } 
                else if( response.startsWith("CORRECT") ) {
                    System.out.println( "Correct letter" );
                } 
                else if( response.startsWith("INCORRECT") ) {
                    System.out.println( "Incorrect letter" );
                } 
                else if( response.startsWith("ALREADY_EXISTS") ) {
                    System.out.println( "The letter was already guessed" );
                }
                else if( response.startsWith("CENSORED_WORD") ) {
                    //Runtime.getRuntime().exec("cls");
                    System.out.println( "Word: " + response.substring(14) );
                } 
                else {
                    System.out.println("Unhandled response: " + response );
                }
                
                output.flush();
            }
        }
        catch(Exception e) {
            System.out.println( e );
        }
        finally {
            socket.close();
        }
    }
    
    public boolean playAgain() {
        System.out.println("Do you want to play again? Y/N");
        return scanner.nextLine().toLowerCase().equals("n");
    }
    
    public static void main( String args[] ) {
        
        while(true) {
            
            try {
                Client client = new Client();
                client.play();
                
                if( client.playAgain() )
                    break;
            }
            catch(Exception e) {
                System.out.println( e );
            }
            
        }
        
    }
    
}
